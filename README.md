# Main digi-merc.org website

[![pipeline status](https://gitlab.com/digi-merc/dm-hugo/badges/master/pipeline.svg)](https://gitlab.com/digi-merc/dm-hugo/commits/master)

Pipeline automated CD for website. Pushes master artifacts to Gitlab pages.


Built with [Hugo](gohugo.io)  
Theme based on [landing-page-hugo](http://themes.gohugo.io/landing-page-hugo/) by crakjie  
updated to include font-awesome 5.10.1  
Colors and some menus / text changed to fit application  
