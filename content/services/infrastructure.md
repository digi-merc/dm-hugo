+++
type = "page"
author = "will"
title = "Infrastructure"  
img = "services/hybrid.png"
+++

  <ul>
    <li>secure remote access</li>
    <li>cloud and hybrid systems</li>
    <li>high-availability / business continuity</li>
    <li>observability</li>
    <li>penetration testing</li>
  </ul>
