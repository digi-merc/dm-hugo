+++
type    = "page"
author  = "will"
title   = "Process Improvement"
img     = "services/lap_pass.png"
+++

  <ul>
    <li>standards compliance</li>
    <li>IT management</li>
    <li>disaster recovery</li>
    <li>incident response</li>
  </ul>
