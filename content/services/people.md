+++
type = "page"
author = "will"
title = "People"
img     = "services/people.png"
+++

  <ul>
    <li>physical security</li>
    <li>awareness training</li>
    <li>insider threat prevention</li>
    <li>digital forensics</li>
    <li>behavioral analysis</li>
  </ul>
