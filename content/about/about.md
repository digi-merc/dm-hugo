+++
type = "page"
author = "will"
title = "On-demand IT help, without the contracts"

+++

Is your company data backed up in the event of a natural disaster or equipment theft? Would your business be able to operate at full capacity if all your employees get quarantined for an extended period of time? These are just a couple of the scenarios I can help with. What was once laughably unthinkable is now very real, in many cases.

I'm offering my 20+ years of IT systems and network experience to the North Central Arkansas area, and beyond. My rates are competitive, and there's no complicated support contracts or vendor lock-in; _everything is on an as-needed basis_.

I can supplement an existing overworked IT staff, or take care of the small tech-related things that inevitably build up that you just need help with. I've done everything from streamlining home internet service to corporate office network build-outs with multi-site VPN connections. On-premise, cloud, or hybrid, Windows, Mac, Linux or anything in between. 

Contact me via the buttons above, and let's get to work.
